package pl.sda.dp.strategy.ex1;

public class HDSettings implements GraphicsSettings {


    @Override
    public void getNeededProccesingPower() {
        System.out.println("High");
    }

    @Override
    public void processFrame(int[][] frame) {
        for (int i = 0; i < frame.length; i++) {
            System.out.println();
            for (int j = 0; j < frame.length; j++) {
                System.out.print(frame[i][j] = 1);
            }
        }
        System.out.println();
    }
}
