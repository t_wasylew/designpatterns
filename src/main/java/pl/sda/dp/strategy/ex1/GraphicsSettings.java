package pl.sda.dp.strategy.ex1;

public interface GraphicsSettings {

    void getNeededProccesingPower();

    void processFrame(int[][] frame);
}
