package pl.sda.dp.strategy.ex1;

public class MediumSetting implements GraphicsSettings {
    @Override
    public void getNeededProccesingPower() {
        System.out.println("Medium");
    }

    @Override
    public void processFrame(int[][] frame) {
        for (int i = 0; i < frame.length; i++) {
            System.out.println();
            for (int j = 0; j < frame.length; j++) {
                System.out.print(frame[i][j] = 2);
            }
        }
        System.out.println();
    }
}
