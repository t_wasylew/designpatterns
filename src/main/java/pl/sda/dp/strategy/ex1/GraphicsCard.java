package pl.sda.dp.strategy.ex1;

public class GraphicsCard {

    private GraphicsSettings graphicsSettings;

    public void setGraphicsSettings(GraphicsSettings graphicsSettings) {
        this.graphicsSettings = graphicsSettings;
    }

    public void getNeededProccsingPower() {
        graphicsSettings.getNeededProccesingPower();
    }

    public void processFrame(int[][] frame) {
        graphicsSettings.processFrame(frame);
    }
}
