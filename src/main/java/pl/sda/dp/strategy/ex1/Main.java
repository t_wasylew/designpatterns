package pl.sda.dp.strategy.ex1;

public class Main {

    public static void main(String[] args) {
        GraphicsCard graphicsCard = new GraphicsCard();

        int[][] frame = new int[5][5];
        graphicsCard.setGraphicsSettings(new LowSetting());
        graphicsCard.getNeededProccsingPower();
        graphicsCard.processFrame(frame);

        graphicsCard.setGraphicsSettings(new HDSettings());
        graphicsCard.getNeededProccsingPower();
        graphicsCard.processFrame(frame);
    }
}
