package pl.sda.dp.strategy.ex2;

import java.util.Scanner;

public class StdinStrategy implements InputStrategy {
    Scanner scanner = new Scanner(System.in);
    @Override
    public int getInt() {
        String s = scanner.nextLine();
        return Integer.parseInt(s);
    }

    @Override
    public String getString() {
        String s = scanner.nextLine();
        return s;
    }

    @Override
    public double getDouble() {
        String s = scanner.nextLine();
        return Double.parseDouble(s);

    }
}
