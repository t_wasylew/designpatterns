package pl.sda.dp.strategy.ex2;

public class InputReader {

    private InputStrategy strategy;

    public void setStrategy(InputStrategy strategy) {
        this.strategy = strategy;
    }

    public int getInt() {
        return strategy.getInt();
    }

    public String getString() {
        return strategy.getString();
    }

    public double getDouble() {
        return strategy.getDouble();
    }
}
