package pl.sda.dp.strategy.ex2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class FileStrategy implements InputStrategy{

    private   BufferedReader bufferedReader;
    @Override
    public int getInt() {
        try {
            bufferedReader = new BufferedReader(new FileReader("File_To_Read.txt"));
        } catch (FileNotFoundException e) {
            System.out.println("File not fouond");
        }
        return 1;
    }

    @Override
    public String getString() {
        return null;
    }

    @Override
    public double getDouble() {
        return 0;
    }
}
