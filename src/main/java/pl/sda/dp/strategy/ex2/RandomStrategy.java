package pl.sda.dp.strategy.ex2;

import java.util.Random;

public class RandomStrategy implements InputStrategy {

   private Random random;

    @Override
    public int getInt() {
        return random.nextInt();
    }

    @Override
    public String getString() {
        char[] chars = new char[random.nextInt(100)];
        for (int i = 0; i < chars.length; i++) {
            chars[i] = (char)random.nextInt(chars.length);
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            stringBuilder.append(chars[i]);
        }
        return stringBuilder.toString();
    }

    @Override
    public double getDouble() {
        return random.nextDouble();
    }
}
