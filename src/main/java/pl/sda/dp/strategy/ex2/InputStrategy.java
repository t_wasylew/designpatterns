package pl.sda.dp.strategy.ex2;

public interface InputStrategy {

    int getInt();
    String getString();
    double getDouble();
}
