package pl.sda.dp.stworzeniowe.abstractfactory.ex3;

import java.time.LocalDateTime;

public class Application {

    private LocalDateTime dataUtworzenia;
    private String miejsceUtworzenia;
    private Person daneAplikanta;
    private String tresc;

    public LocalDateTime getDataUtworzenia() {
        return dataUtworzenia;
    }

    public void setDataUtworzenia(LocalDateTime dataUtworzenia) {
        this.dataUtworzenia = dataUtworzenia;
    }

    public String getMiejsceUtworzenia() {
        return miejsceUtworzenia;
    }

    public void setMiejsceUtworzenia(String miejsceUtworzenia) {
        this.miejsceUtworzenia = miejsceUtworzenia;
    }

    public Person getDaneAplikanta() {
        return daneAplikanta;
    }

    public void setDaneAplikanta(Person daneAplikanta) {
        this.daneAplikanta = daneAplikanta;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }

    public Application(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc) {
        this.dataUtworzenia = dataUtworzenia;
        this.miejsceUtworzenia = miejsceUtworzenia;
        this.daneAplikanta = daneAplikanta;
        this.tresc = tresc;


    }
}
