package pl.sda.dp.stworzeniowe.abstractfactory.ex4;

public class PhoneCall {

    private String callNumber;

    public PhoneCall(String callNumber) {
        this.callNumber = callNumber;
    }

    public String getCallNumber() {
        return callNumber;
    }

    public void setCallNumber(String callNumber) {
        this.callNumber = callNumber;
    }
}
