package pl.sda.dp.stworzeniowe.abstractfactory.ex2;

public class BikeFactory {

    public static Bike create5GeatCross(){
        return new Bike(MARKA.KROSS, 1, 5, BIKE_TYPE.BICYCLE);
    }

    public static Bike create3GearTandemIninaBike() {
        return new Bike(MARKA.ININA, 2, 3, BIKE_TYPE.TANDEM);
    }
}
