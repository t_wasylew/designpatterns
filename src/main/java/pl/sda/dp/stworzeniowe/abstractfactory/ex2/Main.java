package pl.sda.dp.stworzeniowe.abstractfactory.ex2;

public class Main {
    public static void main(String[] args) {

        Bike bike = BikeFactory.create5GeatCross();
        Bike bike1 = BikeFactory.create3GearTandemIninaBike();
    }
}
