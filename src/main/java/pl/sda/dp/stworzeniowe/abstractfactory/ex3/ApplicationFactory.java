package pl.sda.dp.stworzeniowe.abstractfactory.ex3;

import java.time.LocalDateTime;

public abstract class ApplicationFactory {

    public static Application createConditionalStayApplication(Person person, String tresc) {
        return new ConditionalStayApplication(LocalDateTime.now(), "Gdansk", person, tresc);
    }

    public static Application createSchoolarshipApplication(Person person, String text){
        return new SchoolarshipApplication(LocalDateTime.now(), "Gdansk", person, text);
    }

    public static Application createSemesterExtendsApplication(Person person, String s) {
        return new SemesterExtendApplication(LocalDateTime.now(), "Gdansk", person, s);
    }

    public static Application createSocialSchoolarshipApplication(Person person, String text) {
        return new SocialSchoolarshipApplication(LocalDateTime.now(), "Gdansk", person, text);
    }
}
