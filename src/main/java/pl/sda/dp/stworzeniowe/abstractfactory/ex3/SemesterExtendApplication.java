package pl.sda.dp.stworzeniowe.abstractfactory.ex3;

import java.time.LocalDateTime;

public class SemesterExtendApplication extends Application {
    public SemesterExtendApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
    }

    private String powod;
    public String getPowod() {
        return powod;
    }

    public void setPowod(String powod) {
        this.powod = powod;
    }
}
