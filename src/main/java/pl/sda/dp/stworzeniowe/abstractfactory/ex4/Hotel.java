package pl.sda.dp.stworzeniowe.abstractfactory.ex4;

import java.util.ArrayList;
import java.util.List;

public class Hotel {

    private HotelPhone hotelPhone;
    private List<HotelEmployee> hotelEmployees = new ArrayList<>();
    private List<HotelRoom> hotelRooms = new ArrayList<>();
    private List<HotelGuest> hotelGuests = new ArrayList<>();
    private int employeeNumber;
    private int counter = 0;

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    public int getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(int employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public Hotel(HotelPhone hotelPhone) {
        this.hotelPhone = hotelPhone;
        hotelRooms.add(new HotelRoom(1, RoomType.HIGH, true));
        hotelRooms.add(new HotelRoom(2, RoomType.MIDIUM, true));
        hotelRooms.add(new HotelRoom(3, RoomType.LOW, true));
        hotelRooms.add(new HotelRoom(4, RoomType.HIGH, true));
        hotelRooms.add(new HotelRoom(5, RoomType.MIDIUM, true));
        hotelRooms.add(new HotelRoom(6, RoomType.LOW, true));
        hotelEmployees.add(new HotelEmployee("John"));
        hotelEmployees.add(new HotelEmployee("Mike"));
        hotelEmployees.add(new HotelEmployee("Rebeca"));
        hotelEmployees.add(new HotelEmployee("Tom"));
        setEmployeeNumber(hotelEmployees.size());
    }

    public void addEmployee(String name) {
        hotelEmployees.add(new HotelEmployee(name));
        employeeNumber++;
    }

    public void handleClient(HotelGuest hotelGuest) {
        hotelGuests.add(hotelGuest);
        for (int i = 0; i < hotelGuests.size(); i++) {
                System.out.println("Our " + hotelEmployees.get(counter) + " will handle " + hotelGuests.get(i));
                counter++;
            if (counter == hotelEmployees.size() - 1) {
                setCounter(0);
            }
            }
        }
    }

