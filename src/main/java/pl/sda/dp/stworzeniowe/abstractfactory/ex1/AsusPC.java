package pl.sda.dp.stworzeniowe.abstractfactory.ex1;

public class AsusPC extends AbstractPC {

    public AsusPC(String name, COMPUTER_BRAND computer_brand, int cpuPower, double gpuPower, boolean isOberlockerd) {
        super(name, computer_brand, cpuPower, gpuPower, isOberlockerd);
    }

    public static AbstractPC createAsusPC() {
        return new AsusPC("F15", COMPUTER_BRAND.ASUS, 90, 15d, false);
    }

    public static AbstractPC createAsusPC2() {
        return new AsusPC("F16", COMPUTER_BRAND.ASUS, 50, 16d, true);
    }
}
