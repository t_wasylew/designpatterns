package pl.sda.dp.stworzeniowe.abstractfactory.ex4;

public class HotelRoom {

    private int roomNumber;
    private RoomType roomType;
    private boolean isFree;

    public HotelRoom(int roomNumber, RoomType roomType,boolean isFree) {
        this.roomNumber = roomNumber;
        this.roomType = roomType;
        this.isFree = isFree;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }
}
