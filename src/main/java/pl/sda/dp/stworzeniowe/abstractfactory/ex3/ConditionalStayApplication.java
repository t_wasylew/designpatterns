package pl.sda.dp.stworzeniowe.abstractfactory.ex3;

import java.time.LocalDateTime;

public class ConditionalStayApplication extends Application {

    public ConditionalStayApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
    }

    private double[] oceny;
    private String reason;

    public double[] getOceny() {
        return oceny;
    }

    public void setOceny(double[] oceny) {
        this.oceny = oceny;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
