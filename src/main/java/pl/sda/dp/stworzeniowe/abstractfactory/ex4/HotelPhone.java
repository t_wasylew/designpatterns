package pl.sda.dp.stworzeniowe.abstractfactory.ex4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HotelPhone {

    private Map<SpeedDial, String> speedDialMap = new HashMap<>();
    private List<PhoneCall> phoneCallList = new ArrayList<>();

    private void call(HotelGuest hotelGuest, String number) {
        PhoneCall phoneCall = new PhoneCall(number);
        phoneCallList.add(phoneCall);
        System.out.println("Dzwonie do " + hotelGuest.getImie() + "na numer " + number);
    }

    private void speedCall(SpeedDial speedDial, String number) {
        if (number.length() == 4) {
            speedDialMap.put(speedDial,number);
        }
    }
}
