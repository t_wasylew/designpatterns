package pl.sda.dp.stworzeniowe.abstractfactory.ex4;

import java.io.PrintWriter;

public class HotelGuest {

    private String id;
    private String imie;

    public HotelGuest(String id, String imie) {
        this.id = id;
        this.imie = imie;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }
}
