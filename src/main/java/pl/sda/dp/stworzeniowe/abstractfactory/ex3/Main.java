package pl.sda.dp.stworzeniowe.abstractfactory.ex3;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Write name, surname and your Id number: ");
        String string = scanner.nextLine();
        System.out.println();
        String[] splitString = string.trim().split(" ");
        try {
            int idNumber =Integer.parseInt(splitString[2]);
            Person person = new Person(splitString[0], splitString[1], idNumber);
            System.out.print("What type of application you want to write: " +
                    "ConditionalStay/Schoolarship/SemesterExtend/SocialSchoolarship?");
            String application = scanner.nextLine().toLowerCase().trim();

            if (application.startsWith("semester extend")) {
                String text = justifyApplication(scanner);
                Application application1 = ApplicationFactory.createSemesterExtendsApplication(person,text);
                toFile(application1);
            } else {
                double[] gradesDouble = getDoubles(scanner);
                if (application.startsWith("conditional stay")) {
                    String text = justifyApplication(scanner);
                    Application application1 = ApplicationFactory.createConditionalStayApplication
                            (person,text);
                    toFile(application1 , gradesDouble);
                    
                } else if (application.startsWith("schoolarship")) {
                    System.out.print("Write your extracurricular activities (divided by comma): ");
                    String extracurricularActivities = scanner.nextLine();
                    String[] splitedExtracurricularActivities = extracurricularActivities.trim().split(",");
                    String text = justifyApplication(scanner);
                    Application application1 = ApplicationFactory.createSchoolarshipApplication(person, text);
                    toFile(application1, splitedExtracurricularActivities, gradesDouble);
                    
                } else if (application.startsWith("social schoolarship")) {
                    System.out.printf("Write your total Family income: ");
                    String totalFamilyIncome = scanner.nextLine();
                    double totalFamilyIncomeDouble = Double.parseDouble(totalFamilyIncome);
                    String text = justifyApplication(scanner);
                    Application application1 = ApplicationFactory.createSocialSchoolarshipApplication(person, text);
                    toFile(application1, gradesDouble, totalFamilyIncomeDouble);
                }
            }
        } catch (IllegalArgumentException iae) {
            System.out.println("Wrong Id number format");
        }


    }

    private static void toFile(Application application1, double[] gradesDouble, double totalFamilyIncomeDouble) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter("SocialSchoolarApplication.txt"));
            printWriter.println(application1.getDaneAplikanta().toString());
            for (Double grades: gradesDouble) {
                printWriter.print(grades+ ",");
            }
            printWriter.println(totalFamilyIncomeDouble);
            printWriter.println(application1.getTresc());
            printWriter.close();
        } catch (IOException io) {
            System.out.println("Error! Can't generate an application");
        }
    }

    private static void toFile(Application application1, String[] splitedExtracurricularActivities, double[] gradesDouble) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter("SchoolarshipApplication.txt"));
            printWriter.println(application1.getDaneAplikanta().toString());
            for (Double grades: gradesDouble) {                printWriter.print(grades+ ",");
            }
            printWriter.println(splitedExtracurricularActivities);
            printWriter.println(application1.getTresc());
            printWriter.close();
        } catch (IOException io) {
            System.out.println("Error! Can't generate an application");
        }
    }

    private static void toFile(Application application1, double[] gradesDouble) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter("ConditionalStayApplication.txt"));
            printWriter.println(application1.getDaneAplikanta().toString());
            for (Double grades: gradesDouble) {
                printWriter.print(grades+ ",");
            }
            printWriter.println(application1.getTresc());
            printWriter.close();
        } catch (IOException io) {
            System.out.println("Error! Can't generate an application");
        }
    }

    private static void toFile(Application application1) {
        try {
            PrintWriter printWriter = new PrintWriter(new FileWriter("SemesterExtend.txt"));
            printWriter.println(application1.getDaneAplikanta().toString());
            printWriter.println(application1.getTresc());
            printWriter.close();
        } catch (IOException io) {
            System.out.println("Error! Can't generate an application");
        }
    }

    private static double[] getDoubles(Scanner scanner) {
        System.out.print("Write your grades (divided by comma): ");
        String grades = scanner.nextLine();
        String[] splitGrades = grades.split(",");
        return transferStringGradesToDouble(splitGrades);
    }

    private static String justifyApplication(Scanner scanner) {
        System.out.print("Justify your application: ");
        String justify = scanner.nextLine();
        return justify;
    }

    private static double[] transferStringGradesToDouble(String[] splitGrades) {
        double[] grades = new double[splitGrades.length];

        for (int i = 0; i < grades.length; i++) {
            grades[i] = Double.parseDouble(splitGrades[i].trim());
        }
        return grades;
    }
}
