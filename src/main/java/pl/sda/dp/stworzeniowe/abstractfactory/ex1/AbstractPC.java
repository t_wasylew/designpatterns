package pl.sda.dp.stworzeniowe.abstractfactory.ex1;

public abstract class AbstractPC {
    private String name;
    private COMPUTER_BRAND computer_brand;
    private int cpuPower;
    private double gpuPower;
    private boolean isOberlockerd;

    public AbstractPC(String name, COMPUTER_BRAND computer_brand, int cpuPower, double gpuPower, boolean isOberlockerd) {
        this.name = name;
        this.computer_brand = computer_brand;
        this.cpuPower = cpuPower;
        this.gpuPower = gpuPower;
        this.isOberlockerd = isOberlockerd;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
