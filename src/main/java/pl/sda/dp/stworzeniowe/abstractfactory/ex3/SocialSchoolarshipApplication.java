package pl.sda.dp.stworzeniowe.abstractfactory.ex3;

import java.time.LocalDateTime;

public class SocialSchoolarshipApplication extends Application {
    public SocialSchoolarshipApplication(LocalDateTime dataUtworzenia, String miejsceUtworzenia, Person daneAplikanta, String tresc) {
        super(dataUtworzenia, miejsceUtworzenia, daneAplikanta, tresc);
    }

    private double[]oceny;
    private double totalFamilyIncome;

    public double[] getOceny() {
        return oceny;
    }

    public void setOceny(double[] oceny) {
        this.oceny = oceny;
    }

    public double getTotalFamilyIncome() {
        return totalFamilyIncome;
    }

    public void setTotalFamilyIncome(double totalFamilyIncome) {
        this.totalFamilyIncome = totalFamilyIncome;
    }
}
