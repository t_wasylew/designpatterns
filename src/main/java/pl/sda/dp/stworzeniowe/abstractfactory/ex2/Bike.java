package pl.sda.dp.stworzeniowe.abstractfactory.ex2;

public class Bike {

    private MARKA marka;
    private int iloscMiejsc;
    private int iloscPrzerzutek;
    private BIKE_TYPE bike_type;

    public Bike(MARKA marka, int iloscMiejsc, int iloscPrzerzutek, BIKE_TYPE bike_type) {
        this.marka = marka;
        this.iloscMiejsc = iloscMiejsc;
        this.iloscPrzerzutek = iloscPrzerzutek;
        this.bike_type = bike_type;
    }
}
