package pl.sda.dp.stworzeniowe.abstractfactory.ex3;

public class Person {

    private String imie;
    private String nazwisko;
    private int numerIdenksu;

    public Person(String imie, String nazwisko, int numerIdenksu) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.numerIdenksu = numerIdenksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public int getNumerIdenksu() {
        return numerIdenksu;
    }

    public void setNumerIdenksu(int numerIdenksu) {
        this.numerIdenksu = numerIdenksu;
    }

    @Override
    public String toString() {
        return "Person{" +
                "imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", numerIdenksu=" + numerIdenksu +
                '}';
    }
}
