package pl.sda.dp.stworzeniowe.singleton.ex1;

public class HealthDepartment {

    private WaitingRoomMachine machine = new WaitingRoomMachine();
    private Reception cashRegistry = new Reception();

    public void generateTicketMachine() {
        System.out.println(machine.generateTicket());
    }

    public void generateTicketReception() {
        System.out.println(cashRegistry.generateTicket());
    }

}
