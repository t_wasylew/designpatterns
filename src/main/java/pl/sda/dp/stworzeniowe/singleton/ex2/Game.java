package pl.sda.dp.stworzeniowe.singleton.ex2;

import java.util.Random;
import java.util.Scanner;


public class Game {
    public Random random = new Random();
    Scanner scanner = new Scanner(System.in);
    public void run() {
        int maxRund = MySettings.getInstance().getIloscRund();
        int runda = 1;
        int liczba1;
        int liczba2;
        int dzialanie;
        do {
            liczba1 = random.nextInt(MySettings.getInstance().getZakres1());
            liczba2 = random.nextInt(MySettings.getInstance().getZakres2());
            dzialanie = random.nextInt(MySettings.getInstance().getDzialania().length);
            String jednoDzialanie = MySettings.getInstance().getDzialania()[dzialanie];

            System.out.printf("Runda %d \n", runda);
            System.out.printf("Ile wynosi %d %s %d \n", liczba1, jednoDzialanie, liczba2);
            System.out.print("Podaj wynik: ");
            int wynik = scanner.nextInt();

            int wynikDzialania=0;
            if (jednoDzialanie.startsWith("-")) {
                wynikDzialania = liczba1-liczba2;
            } else if (jednoDzialanie.startsWith("+")) {
                wynikDzialania = liczba1 + liczba2;
            } else if (jednoDzialanie.startsWith("/")) {
                wynikDzialania = liczba1 / liczba2;
            } else if (jednoDzialanie.startsWith("*")) {
                wynikDzialania = liczba1 * liczba2;
            }

            if (wynik == wynikDzialania) {
                System.out.println("Brawo");
            }else {
                System.out.println("Źle");
            }
            runda++;

        } while (maxRund != runda);
    }
}
