package pl.sda.dp.stworzeniowe.singleton.ex1;

public class Ticket {
    private String origin;
    private int ticketNumber;

    public Ticket(String origin, int ticketNumber) {
        this.origin = origin;
        this.ticketNumber = ticketNumber;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "origin='" + origin + '\'' +
                ", ticketNumber=" + ticketNumber +
                '}';
    }
}
