package pl.sda.dp.stworzeniowe.singleton.ex2;

public class Main {

    public static void main(String[] args) {
        FileReader  fileReader = new FileReader();
        MySettings.getInstance().setConfig((fileReader.readFile("config.txt")));
        Game game = new Game();
        game.run();

    }
}
