package pl.sda.dp.stworzeniowe.singleton.ex2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileReader {

    public List<String> readFile(String fileNameToRead) {
        List<String> lines = new ArrayList<String>();
        try {
            Scanner scanner = new Scanner(new File(fileNameToRead));
            while (scanner.hasNext()) {
                lines.add(scanner.nextLine());
            }
//            MySettings.getInstance().setIloscRund(Integer.parseInt(scanner.nextLine().split("=")[1]));
        } catch (FileNotFoundException e) {
            System.out.println("Error");
        }

        return lines;
    }
}
