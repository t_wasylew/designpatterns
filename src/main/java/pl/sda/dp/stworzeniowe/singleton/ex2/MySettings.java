package pl.sda.dp.stworzeniowe.singleton.ex2;

import java.util.List;

public class MySettings {

    private static MySettings ourInstance = new MySettings();

    public static MySettings getInstance() {
        return ourInstance;
    }

    private int zakres1;
    private int zakres2;
    private String[] dzialania;
    private int iloscRund;

    public int getZakres1() {
        return zakres1;
    }

    public void setZakres1(int zakres1) {
        this.zakres1 = zakres1;
    }

    public int getZakres2() {
        return zakres2;
    }

    public void setZakres2(int zakres2) {
        this.zakres2 = zakres2;
    }

    public String[] getDzialania() {
        return dzialania;
    }

    public void setDzialania(String[] dzialania) {
        this.dzialania = dzialania;
    }

    public int getIloscRund() {
        return iloscRund;
    }

    public void setIloscRund(int iloscRund) {
        this.iloscRund = iloscRund;
    }

    public void setConfig(List<String> list) {
        String[] zakres1 = list.get(0).split("=");
        setZakres1(Integer.parseInt(zakres1[1]));

        String[] zakres2 = list.get(1).split("=");
        setZakres2(Integer.parseInt(zakres2[1]));

        String[] dzialanie = list.get(2).split("=");
        String[]dzialania = dzialanie[1].split("");
        setDzialania(dzialania);

        String[] liczbaRund = list.get(3).split("=");
        setIloscRund(Integer.parseInt(liczbaRund[1]));

    }
}
