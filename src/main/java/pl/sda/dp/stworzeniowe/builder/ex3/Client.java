package pl.sda.dp.stworzeniowe.builder.ex3;

import java.util.ArrayList;
import java.util.List;

public class Client {

    private String name;
    private List<Mail> mailList = new ArrayList<>();

    public Client(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void readMail(Mail mail) {
        mailList.add(mail);
        System.out.println("Klient" + this.getName() + "otrzymal maila" + mail.getTresc());
    }
}
