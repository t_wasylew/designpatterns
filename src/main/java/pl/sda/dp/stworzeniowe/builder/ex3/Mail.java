package pl.sda.dp.stworzeniowe.builder.ex3;

import java.time.LocalDate;

public class Mail {

    private String tresc;
    private String nadawca;
    private LocalDate dataNadania;
    private LocalDate dataOdbioru;
    private String ipNadania;
    private String ipOdebrania;
    private String nazwaSerweraPosredniego;
    private String nazwaSkrzynkiPocztowej;
    private String protokolKomunikacji;
    private TYP_WIADOMOSCI TYPWIADOMOSCI;
    private boolean czySzyfrowane;
    private boolean czySpam;

    public Mail(String tresc, String nadawca, LocalDate dataNadania, LocalDate dataOdbioru, String ipNadania, String ipOdebrania, String nazwaSerweraPosredniego, String nazwaSkrzynkiPocztowej, String protokolKomunikacji, TYP_WIADOMOSCI TYPWIADOMOSCI, boolean czySzyfrowane, boolean czySpam) {
        this.tresc = tresc;
        this.nadawca = nadawca;
        this.dataNadania = dataNadania;
        this.dataOdbioru = dataOdbioru;
        this.ipNadania = ipNadania;
        this.ipOdebrania = ipOdebrania;
        this.nazwaSerweraPosredniego = nazwaSerweraPosredniego;
        this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
        this.protokolKomunikacji = protokolKomunikacji;
        this.TYPWIADOMOSCI = TYPWIADOMOSCI;
        this.czySzyfrowane = czySzyfrowane;
        this.czySpam = czySpam;
    }

    public String getTresc() {
        return tresc;
    }

    public static class Builder {
        private String tresc;
        private String nadawca;
        private LocalDate dataNadania;
        private LocalDate dataOdbioru;
        private String ipNadania;
        private String ipOdebrania;
        private String nazwaSerweraPosredniego;
        private String nazwaSkrzynkiPocztowej;
        private String protokolKomunikacji;
        private TYP_WIADOMOSCI TYPWIADOMOSCI;
        private boolean czySzyfrowane;
        private boolean czySpam;

        public Builder setTresc(String tresc) {
            this.tresc = tresc;
            return this;
        }

        public Builder setNadawca(String nadawca) {
            this.nadawca = nadawca;
            return this;
        }

        public Builder setDataNadania(LocalDate dataNadania) {
            this.dataNadania = dataNadania;
            return this;
        }

        public Builder setDataOdbioru(LocalDate dataOdbioru) {
            this.dataOdbioru = dataOdbioru;
            return this;
        }

        public Builder setIpNadania(String ipNadania) {
            this.ipNadania = ipNadania;
            return this;
        }

        public Builder setIpOdebrania(String ipOdebrania) {
            this.ipOdebrania = ipOdebrania;
            return this;
        }

        public Builder setNazwaSerweraPosredniego(String nazwaSerweraPosredniego) {
            this.nazwaSerweraPosredniego = nazwaSerweraPosredniego;
            return this;
        }

        public Builder setNazwaSkrzynkiPocztowej(String nazwaSkrzynkiPocztowej) {
            this.nazwaSkrzynkiPocztowej = nazwaSkrzynkiPocztowej;
            return this;
        }

        public Builder setProtokolKomunikacji(String protokolKomunikacji) {
            this.protokolKomunikacji = protokolKomunikacji;
            return this;
        }

        public Builder setTYPWIADOMOSCI(TYP_WIADOMOSCI TYPWIADOMOSCI) {
            this.TYPWIADOMOSCI = TYPWIADOMOSCI;
            return this;
        }

        public Builder setCzySzyfrowane(boolean czySzyfrowane) {
            this.czySzyfrowane = czySzyfrowane;
            return this;
        }

        public Builder setCzySpam(boolean czySpam) {
            this.czySpam = czySpam;
            return this;
        }

        public Mail createMail() {
            return new Mail(tresc, nadawca, dataNadania, dataOdbioru, ipNadania, ipOdebrania, nazwaSerweraPosredniego, nazwaSkrzynkiPocztowej, protokolKomunikacji, TYPWIADOMOSCI, czySzyfrowane, czySpam);
        }
    }

}
