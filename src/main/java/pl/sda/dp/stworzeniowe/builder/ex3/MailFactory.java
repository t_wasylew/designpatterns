package pl.sda.dp.stworzeniowe.builder.ex3;

import java.time.LocalDate;

public class MailFactory {

    public static Mail createOfferMail(String offer) {
        return new Mail.Builder().setCzySpam(false).setCzySzyfrowane(false).setDataNadania(LocalDate.now())
                .setNadawca("Tomek").setTresc("Bla bla bla").setTYPWIADOMOSCI(TYP_WIADOMOSCI.OFFER).createMail();
    }

    public static Mail createForumMail(String tresc) {
        return new Mail.Builder().setTYPWIADOMOSCI(TYP_WIADOMOSCI.FORUM).setNadawca("Forum").setTresc("Bla bla bla")
                .setDataNadania(LocalDate.now()).setCzySzyfrowane(false).setCzySpam(false).createMail();
    }
}
