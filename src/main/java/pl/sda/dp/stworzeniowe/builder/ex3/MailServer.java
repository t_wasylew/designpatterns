package pl.sda.dp.stworzeniowe.builder.ex3;

import java.util.ArrayList;
import java.util.List;

public class MailServer {

    private List<Client> connectedClient = new ArrayList<>();

    public void connect(Client client) {
        connectedClient.add(client);
    }

    public void disconnect(Client client) {
        if (connectedClient.contains(client)) {
            connectedClient.remove(client);
        }
    }

    public void sendMassage(Mail mail, Client sender) {
        for (Client client : connectedClient) {
            if (!client.equals(sender)) {
                client.readMail(mail);
            }
        }
    }
}
