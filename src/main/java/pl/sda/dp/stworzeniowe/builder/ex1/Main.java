package pl.sda.dp.stworzeniowe.builder.ex1;

        import java.util.ArrayList;
        import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<GameCharacter> gameCharacterList = new ArrayList<GameCharacter>();
        gameCharacterList.add(new GameCharacter.Builder().setHealth(120).setMana(100).setName("Arek").setNumberOfPoints(99).createGameCharacter());
        gameCharacterList.add(new GameCharacter.Builder().setHealth(121).setMana(100).setName("Arek").setNumberOfPoints(3).createGameCharacter());
        gameCharacterList.add(new GameCharacter.Builder().setHealth(122).setName("Arek").setNumberOfPoints(92).createGameCharacter());
        gameCharacterList.add(new GameCharacter.Builder().setHealth(123).setMana(100).setName("Arek").setNumberOfPoints(1).createGameCharacter());
        gameCharacterList.add(new GameCharacter.Builder().setHealth(124).setName("Arek").setNumberOfPoints(91).createGameCharacter());
        gameCharacterList.add(new GameCharacter.Builder().setHealth(125).setMana(100).setName("Arek").setNumberOfPoints(6).createGameCharacter());

        for (GameCharacter game: gameCharacterList) {
            System.out.println(game);
        }
    }
}
