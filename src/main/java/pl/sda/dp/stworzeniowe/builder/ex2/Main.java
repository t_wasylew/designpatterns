package pl.sda.dp.stworzeniowe.builder.ex2;

public class Main {

    public static void main(String[] args) {

        Stamp.Builder stamp = new Stamp.Builder();
        Stamp stamp1 = stamp.createStamp();
        System.out.println(stamp1);
        Stamp stamp2 = stamp.setYearNumber2(2).setCaseNumber(2).setYearNumber1(5).createStamp();
        System.out.println(stamp2);
    }
}
