package pl.sda.dp.facade.ex1.frother;

import pl.sda.dp.facade.ex1.MilkIngredient;

public class MilkFrother {

    public FoamedMilkIngredient froth(MilkIngredient milkIngredient) throws MilkFrotherException {
        if (Math.random() > 0.8) {
            throw new MilkFrotherException("Frtoher Error");
        }
        if (!(milkIngredient == null)){
                return new FoamedMilkIngredient();
            }else{
            throw new MilkFrotherException("No milk detected.");
        }
    }
}
