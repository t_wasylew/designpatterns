package pl.sda.dp.facade.ex1.grinder;

public class CoffieGrinderException extends Exception {


    public CoffieGrinderException(String message) {
        super(message);
    }
}
