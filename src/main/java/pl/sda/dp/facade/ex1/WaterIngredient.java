package pl.sda.dp.facade.ex1;

public class WaterIngredient extends CoffeeIngredient {

    private int water;

    public WaterIngredient(int water) {
        this.water = water;
    }

    public int getWater() {
        return water;
    }

    public void setWater(int water) {
        this.water = water;
    }
}
