package pl.sda.dp.facade.ex1.barista;

import pl.sda.dp.facade.ex1.*;
import pl.sda.dp.facade.ex1.frother.MilkFrother;
import pl.sda.dp.facade.ex1.frother.MilkFrotherException;
import pl.sda.dp.facade.ex1.grinder.CoffieGrinder;
import pl.sda.dp.facade.ex1.grinder.CoffieGrinderException;

public class BaristaMain {

    public static void main(String[] args) {
        CoffieGrinder coffieGrinder = new CoffieGrinder();
        MilkFrother milkFrother = new MilkFrother();

        CoffeeBeverage espresso = new CoffeeBeverage();
        try {
            espresso.getCoffeeIngredientList().add(coffieGrinder.grind(new CoffieBean()));
            espresso.getCoffeeIngredientList().add(new WaterIngredient(30));
            System.out.println(espresso.toString());
        } catch (CoffieGrinderException e) {
            e.printStackTrace();
        }


        CoffeeBeverage latte = new CoffeeBeverage();
        try {
            latte.getCoffeeIngredientList().add(coffieGrinder.grind(new CoffieBean()));
            latte.getCoffeeIngredientList().add(new WaterIngredient(100));
            latte.getCoffeeIngredientList().add(milkFrother.froth(new MilkIngredient()));
            latte.getCoffeeIngredientList().add(new SugarIngredient());
            System.out.println(latte.toString());
        } catch (CoffieGrinderException | MilkFrotherException e) {
            e.printStackTrace();
        }

        CoffeeBeverage espresso1 = new CoffeeBeverage();
        try {
            espresso1.getCoffeeIngredientList().add(coffieGrinder.grind(new CoffieBean()));
            espresso1.getCoffeeIngredientList().add(new WaterIngredient(30));
            System.out.println(espresso1.toString());
        } catch (CoffieGrinderException e) {
            e.printStackTrace();
        }

        CoffeeBeverage latte1 = new CoffeeBeverage();
        try {
            latte1.getCoffeeIngredientList().add(coffieGrinder.grind(new CoffieBean()));
            latte1.getCoffeeIngredientList().add(new WaterIngredient(100));
            latte1.getCoffeeIngredientList().add(milkFrother.froth(new MilkIngredient()));
            latte1.getCoffeeIngredientList().add(new SugarIngredient());
            System.out.println(latte1.toString());
        } catch (CoffieGrinderException | MilkFrotherException e) {
            e.printStackTrace();
        }

        coffieGrinder.empty();

        CoffeeBeverage latte2 = new CoffeeBeverage();
        try {
            latte2.getCoffeeIngredientList().add(coffieGrinder.grind(new CoffieBean()));
            latte2.getCoffeeIngredientList().add(new WaterIngredient(100));
            latte2.getCoffeeIngredientList().add(milkFrother.froth(new MilkIngredient()));
            latte2.getCoffeeIngredientList().add(new SugarIngredient());
            System.out.println(latte2.toString());
        } catch (CoffieGrinderException | MilkFrotherException e) {
            e.printStackTrace();
        }
    }
}
