package pl.sda.dp.facade.ex1;

public class Main {

    public static void main(String[] args) {
        SimpleCoffeeMachine simpleCoffeeMachine = new SimpleCoffeeMachine();

        simpleCoffeeMachine.makeCappuccino();
        simpleCoffeeMachine.makeAmericana();
        simpleCoffeeMachine.makeEspresso();
        simpleCoffeeMachine.makeLatte();
    }
}
