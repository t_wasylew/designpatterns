package pl.sda.dp.facade.ex1;

import java.util.ArrayList;
import java.util.List;

public class CoffeeBeverage {

    private List<CoffeeIngredient> coffeeIngredientList = new ArrayList<>();

    public CoffeeBeverage() {
        this.coffeeIngredientList = coffeeIngredientList;
    }

    public List<CoffeeIngredient> getCoffeeIngredientList() {
        return coffeeIngredientList;
    }

    public void setCoffeeIngredientList(List<CoffeeIngredient> coffeeIngredientList) {
        this.coffeeIngredientList = coffeeIngredientList;
    }

    @Override
    public String toString() {
        return "CoffeeBeverage{" +
                "coffeeIngredientList=" + coffeeIngredientList +
                '}';
    }
}
