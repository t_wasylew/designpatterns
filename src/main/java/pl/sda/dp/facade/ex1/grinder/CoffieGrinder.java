package pl.sda.dp.facade.ex1.grinder;

import pl.sda.dp.facade.ex1.CoffieBean;

public class CoffieGrinder {

    private int wasteContainerCount;

    public CoffieGrinder() {
    }

    public int getWasteContainerCount() {
        return wasteContainerCount;
    }

    public void setWasteContainerCount(int wasteContainerCount) {
        this.wasteContainerCount = wasteContainerCount;
    }

    public void empty() {
        setWasteContainerCount(0);
    }

    public CoffeePowderIngredient grind (CoffieBean coffieBean) throws CoffieGrinderException {

        if (wasteContainerCount > 3) {
            throw new CoffieGrinderException("Grinder full!");
        } else if (!(coffieBean == null)){
            System.out.println("Waste level in grinder: " + wasteContainerCount);
            wasteContainerCount++;
            return new CoffeePowderIngredient();
        }else {
            throw new CoffieGrinderException("No beans detected in grinder");
        }
    }
}
