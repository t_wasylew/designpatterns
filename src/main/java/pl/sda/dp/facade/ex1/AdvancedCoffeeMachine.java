package pl.sda.dp.facade.ex1;

import java.util.ArrayList;
import java.util.List;

public class AdvancedCoffeeMachine {

    private List<CoffeeIngredient> ingredientList = new ArrayList<>();

    public List<CoffeeIngredient> getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(List<CoffeeIngredient> ingredientList) {
        this.ingredientList = ingredientList;
    }

    public void addIngredient(CoffeeIngredient coffeeIngredient) {
        ingredientList.add(coffeeIngredient);
    }

    public CoffeeBeverage makeCoffeeBaverage(List<CoffeeIngredient> ingrediensList) {
        if (!ingrediensList.isEmpty()) {
            CoffeeBeverage coffeeBeverage = new CoffeeBeverage();
            for (CoffeeIngredient ingredient: ingrediensList) {
                coffeeBeverage.getCoffeeIngredientList().add(ingredient);
            }
            ingrediensList.clear();
            return coffeeBeverage;
        }else {
            throw new NullPointerException();
        }
    }
}
