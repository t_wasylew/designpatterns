package pl.sda.dp.facade.ex1;

import pl.sda.dp.facade.ex1.frother.FoamedMilkIngredient;
import pl.sda.dp.facade.ex1.frother.MilkFrother;
import pl.sda.dp.facade.ex1.frother.MilkFrotherException;
import pl.sda.dp.facade.ex1.grinder.CoffeePowderIngredient;
import pl.sda.dp.facade.ex1.grinder.CoffieGrinder;
import pl.sda.dp.facade.ex1.grinder.CoffieGrinderException;

public class SimpleCoffeeMachine {
    private AdvancedCoffeeMachine advancedCoffeeMachine = new AdvancedCoffeeMachine();
    private CoffieGrinder coffieGrinder = new CoffieGrinder();
    private MilkFrother milkFrother = new MilkFrother();

    public CoffeeBeverage makeEspresso() {
        advancedCoffeeMachine.addIngredient(getCoffeePowderIngredient());
        advancedCoffeeMachine.addIngredient(new WaterIngredient(30));
        CoffeeBeverage coffeeBeverage = advancedCoffeeMachine.makeCoffeeBaverage(advancedCoffeeMachine.getIngredientList());
        System.out.println(coffeeBeverage.toString());
        return coffeeBeverage;
    }

    public CoffeeBeverage makeLatte() {
        advancedCoffeeMachine.addIngredient(getCoffeePowderIngredient());
        advancedCoffeeMachine.addIngredient(new WaterIngredient(100));
        advancedCoffeeMachine.addIngredient(makeFoamMilk());
        advancedCoffeeMachine.addIngredient(new SugarIngredient());
        CoffeeBeverage coffeeBeverage = advancedCoffeeMachine.makeCoffeeBaverage(advancedCoffeeMachine.getIngredientList());
        System.out.println(coffeeBeverage.toString());
        return coffeeBeverage;
    }

    public CoffeeBeverage makeCappuccino() {
        advancedCoffeeMachine.addIngredient(getCoffeePowderIngredient());
        advancedCoffeeMachine.addIngredient(new WaterIngredient(30));
        advancedCoffeeMachine.addIngredient(makeFoamMilk());
        advancedCoffeeMachine.addIngredient(new MilkIngredient());
        advancedCoffeeMachine.addIngredient(new SugarIngredient());
        CoffeeBeverage coffeeBeverage = advancedCoffeeMachine.makeCoffeeBaverage(advancedCoffeeMachine.getIngredientList());
        System.out.println(coffeeBeverage.toString());
        return coffeeBeverage;
    }

    public CoffeeBeverage makeAmericana() {
        advancedCoffeeMachine.addIngredient(getCoffeePowderIngredient());
        advancedCoffeeMachine.addIngredient(new WaterIngredient(100));
        CoffeeBeverage coffeeBeverage = advancedCoffeeMachine.makeCoffeeBaverage(advancedCoffeeMachine.getIngredientList());
        System.out.println(coffeeBeverage.toString());
        return coffeeBeverage;
    }

    private CoffeePowderIngredient getCoffeePowderIngredient() {
        CoffeePowderIngredient grind = null;
        while(grind == null) {
            try {
                grind = coffieGrinder.grind(new CoffieBean());
            } catch (CoffieGrinderException e) {
                coffieGrinder.empty();
                System.out.println("Waste cleaned!!!");
            }
        }
        return grind;
    }

    private FoamedMilkIngredient makeFoamMilk() {
        FoamedMilkIngredient foam = null;
        while (foam == null) {
            try {
                foam = milkFrother.froth(new MilkIngredient());
            } catch (MilkFrotherException e) {
                e.printStackTrace();
            }
        }
        return foam;
    }
}
