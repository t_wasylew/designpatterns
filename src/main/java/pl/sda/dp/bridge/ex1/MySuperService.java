package pl.sda.dp.bridge.ex1;

public class MySuperService {

    private IPrinter printer;

    public MySuperService(IPrinter printer) {
        this.printer = printer;
    }

    public void printData(String s) {
        printer.printMessage(s);
    }
}
