package pl.sda.dp.bridge.ex1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class MyFilePrinter implements IPrinter {
    @Override
    public void printMessage(String s) {
        try {
            FileWriter fileWriter = new FileWriter(new File("imie.txt"));
            fileWriter.append(s);
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
