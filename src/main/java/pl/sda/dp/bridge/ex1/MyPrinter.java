package pl.sda.dp.bridge.ex1;

public class MyPrinter implements IPrinter{
    @Override
    public void printMessage(String s) {
        System.out.println(s);
    }
}
