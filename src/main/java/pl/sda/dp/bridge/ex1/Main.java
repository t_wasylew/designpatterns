package pl.sda.dp.bridge.ex1;

public class Main {

    public static void main(String[] args) {
        MySuperService superService = new MySuperService(new MyFilePrinter());
        superService.printData("Tomek");
        MySuperService service = new MySuperService(new MyPrinter());
        service.printData("Tomek");

    }
}
