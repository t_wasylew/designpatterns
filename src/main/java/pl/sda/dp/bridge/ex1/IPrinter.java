package pl.sda.dp.bridge.ex1;

public interface IPrinter {

    void printMessage(String s);
}
