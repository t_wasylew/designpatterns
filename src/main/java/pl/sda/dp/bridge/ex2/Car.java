package pl.sda.dp.bridge.ex2;

public class Car implements IVehicle{
    private String name;

    public Car(String name) {
        this.name = name;
    }

    @Override
    public void moveRight() {
        System.out.println(name + " move right");
    }

    @Override
    public void moveLeft() {
        System.out.println(name + " move left");
    }

    @Override
    public void moveUp() {
        System.out.println(name + " move up");
    }

    @Override
    public void moveDown() {
        System.out.println(name + " move down");
    }
}
