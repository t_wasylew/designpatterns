package pl.sda.dp.event_dispatcher;

import pl.sda.dp.event_dispatcher.application.CallStartRecordedApplication;
import pl.sda.dp.event_dispatcher.application.PhoneApplication;
import pl.sda.dp.event_dispatcher.event.CallEndedEvent;
import pl.sda.dp.event_dispatcher.event.CallStartedEvent;
import pl.sda.dp.event_dispatcher.listeners.ICallEndedListener;
import pl.sda.dp.event_dispatcher.listeners.ICallOutgoingListener;
import pl.sda.dp.event_dispatcher.listeners.ICallStartListener;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class AndroidSystem implements ICallStartListener, ICallEndedListener, ICallOutgoingListener {
    private List<Notification> listOfNotifications;
    private List<Integer> tempIdList;


    public AndroidSystem() {
        this.tempIdList = new ArrayList<>();
        this.listOfNotifications = new LinkedList<>();
        new CallStartRecordedApplication();
        new PhoneApplication();
        EventDispatcher.instance.registerObject(this);

    }

    @Override
    public void callStarted(int call_id) {
        tempIdList.add(call_id);
        System.out.println("System notification - call started with number: " + call_id);
    }

    @Override
    public void callEnded(int call_id) {
        tempIdList.remove((Integer) call_id);
        System.out.println("System notification - call ended with nuber: " + call_id);
    }

    @Override
    public void outgoingCall(int call_id) {
        if (tempIdList.size() < 3) {
            System.out.println("System notification - outgoing call to number: " + call_id);
        }
    }

    public void startCall(int call_id) {
        if (tempIdList.size() < 3) {
            EventDispatcher.instance.dispatch(new CallStartedEvent(call_id));
        } else {
            System.out.println("Busy here!");
        }
    }


    public void stopCall(int call_id) {
        for (Integer id : tempIdList) {
            if (id == call_id) {
                EventDispatcher.instance.dispatch(new CallEndedEvent(call_id));
            } else {
                System.out.println("There is no such connection.");
            }
        }
    }
}
