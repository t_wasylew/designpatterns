package pl.sda.dp.event_dispatcher.application;

import pl.sda.dp.event_dispatcher.EventDispatcher;
import pl.sda.dp.event_dispatcher.listeners.ICallEndedListener;
import pl.sda.dp.event_dispatcher.listeners.ICallOutgoingListener;
import pl.sda.dp.event_dispatcher.listeners.ICallStartListener;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;

public class PhoneApplication implements ICallStartListener, ICallEndedListener, ICallOutgoingListener {
    private List<CallRegistry> registryList = new LinkedList<>();

    public PhoneApplication() {
        EventDispatcher.instance.registerObject(this);

    }

    @Override
    public void callStarted(int call_id) {
        registryList.add(new CallRegistry(call_id, LocalDate.now()));
        System.out.println("PhoneApp- call started: " + call_id);

    }

    @Override
    public void callEnded(int call_id) {
        System.out.println("PhoneApp- call ended: " + call_id );
        for (CallRegistry call : registryList) {
            if (call.getCall_id() == call_id) {
                call.setEndCallDate(LocalDate.now());
            }
        }
    }

    @Override
    public void outgoingCall(int call_id) {
        System.out.println("PhoneApp- ongoing call: " + call_id);
        registryList.add(new CallRegistry(call_id, LocalDate.now(), true));
        }
    }

