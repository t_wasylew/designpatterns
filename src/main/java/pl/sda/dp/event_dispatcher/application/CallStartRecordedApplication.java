package pl.sda.dp.event_dispatcher.application;

import pl.sda.dp.event_dispatcher.EventDispatcher;
import pl.sda.dp.event_dispatcher.listeners.ICallEndedListener;
import pl.sda.dp.event_dispatcher.listeners.ICallOutgoingListener;
import pl.sda.dp.event_dispatcher.listeners.ICallStartListener;

public class CallStartRecordedApplication implements ICallStartListener, ICallEndedListener {

    public CallStartRecordedApplication() {
        EventDispatcher.instance.registerObject(this);
    }

    @Override
    public void callStarted(int call_id) {
        System.out.println("CallRecordApp - call started: " + call_id);

    }

    @Override
    public void callEnded(int call_id) {
        System.out.println("CallRecordApp - call ended: " + call_id);
    }

}
