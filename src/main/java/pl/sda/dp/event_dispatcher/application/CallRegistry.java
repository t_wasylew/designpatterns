package pl.sda.dp.event_dispatcher.application;

import java.time.LocalDate;

public class CallRegistry {
    private int call_id;
    private LocalDate startCallDate;
    private LocalDate endCallDate;
    private boolean isOngoing;

    public CallRegistry(int call_id, LocalDate startCallDate, boolean isOngoing) {
        this.call_id = call_id;
        this.startCallDate = startCallDate;
        this.isOngoing = isOngoing;
    }

    public CallRegistry(int call_id, LocalDate startCallDate) {
        this.call_id = call_id;
        this.startCallDate = startCallDate;
        this.isOngoing = false;
    }

    public int getCall_id() {
        return call_id;
    }

    public void setCall_id(int call_id) {
        this.call_id = call_id;
    }

    public LocalDate getStartCallDate() {
        return startCallDate;
    }

    public void setStartCallDate(LocalDate startCallDate) {
        this.startCallDate = startCallDate;
    }

    public LocalDate getEndCallDate() {
        return endCallDate;
    }

    public void setEndCallDate(LocalDate endCallDate) {
        this.endCallDate = endCallDate;
    }

    public boolean isOngoing() {
        return isOngoing;
    }

    public void setOngoing(boolean ongoing) {
        isOngoing = ongoing;
    }
}
