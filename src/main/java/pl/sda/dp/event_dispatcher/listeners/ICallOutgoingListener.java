package pl.sda.dp.event_dispatcher.listeners;

public interface ICallOutgoingListener {
    void outgoingCall(int call_id);
}
