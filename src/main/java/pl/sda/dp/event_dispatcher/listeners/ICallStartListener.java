package pl.sda.dp.event_dispatcher.listeners;

public interface ICallStartListener {
    void callStarted(int call_id);
}
