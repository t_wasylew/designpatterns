package pl.sda.dp.event_dispatcher.listeners;

public interface ICallEndedListener {
    void callEnded(int call_id);
}
