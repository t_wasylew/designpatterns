package pl.sda.dp.event_dispatcher;

import com.sun.org.apache.xpath.internal.SourceTree;
import com.sun.org.apache.xpath.internal.operations.And;
import pl.sda.dp.event_dispatcher.event.CallEndedEvent;
import pl.sda.dp.event_dispatcher.event.CallStartedEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        AndroidSystem androidSystem = new AndroidSystem();
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String command = line.split(" ")[0];
            int call_id = Integer.parseInt(line.split(" ")[1]);
            if (command.equalsIgnoreCase("start")) {
                androidSystem.startCall(call_id);
            } else if (command.equalsIgnoreCase("stop")) {
                androidSystem.stopCall(call_id);
            }
        }
    }
}

