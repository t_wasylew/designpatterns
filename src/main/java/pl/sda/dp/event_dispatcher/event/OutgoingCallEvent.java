package pl.sda.dp.event_dispatcher.event;

import pl.sda.dp.event_dispatcher.EventDispatcher;
import pl.sda.dp.event_dispatcher.listeners.ICallOutgoingListener;

import java.util.List;

public class OutgoingCallEvent implements IEvent{
    private int id_call;
    private int dialNumber = 0;

    public OutgoingCallEvent(int id_call, int dialNumber) {
        this.id_call = id_call;
        this.dialNumber = dialNumber;
    }

    public OutgoingCallEvent(int id_call) {
        this.id_call = id_call;
    }

    @Override
    public void run() {
        List<ICallOutgoingListener> listeners = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallOutgoingListener.class);
        for (ICallOutgoingListener listener:listeners) {
            listener.outgoingCall(id_call);
        }
    }
}
