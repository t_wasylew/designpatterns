package pl.sda.dp.event_dispatcher.event;

public interface IEvent {
    void run();
}
