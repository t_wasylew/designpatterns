package pl.sda.dp.event_dispatcher.event;

import pl.sda.dp.event_dispatcher.EventDispatcher;
import pl.sda.dp.event_dispatcher.listeners.ICallStartListener;

import java.util.List;

public class CallStartedEvent implements IEvent {

    private int call_id;

    public CallStartedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallStartListener> listeners = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallStartListener.class);
            for (ICallStartListener listener: listeners) {
            listener.callStarted(call_id);
        }
    }
}
