package pl.sda.dp.event_dispatcher.event;

import pl.sda.dp.event_dispatcher.EventDispatcher;
import pl.sda.dp.event_dispatcher.listeners.ICallEndedListener;

import java.util.List;

public class CallEndedEvent implements IEvent {
    private int call_id;

    public CallEndedEvent(int call_id) {
        this.call_id = call_id;
    }

    @Override
    public void run() {
        List<ICallEndedListener> listeners = EventDispatcher.instance.getAllObjectsImplementingInterface(ICallEndedListener.class);
        for (ICallEndedListener listener:listeners) {
            listener.callEnded(call_id);
        }
    }
}
