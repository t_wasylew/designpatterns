package pl.sda.dp.event_dispatcher;

public class Notification {

    private String notificationText;

    public Notification(String notificationText) {
        this.notificationText = notificationText;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }
}
