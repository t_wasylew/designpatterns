package pl.sda.dp.dekorator.ex2;

public class Main {

    public static void main(String[] args) {
        IPizza pizza = new SimplePizza(20);
        System.out.println(pizza.toString());
        pizza = PizzaFactory.createPepperoniPizza(pizza);
        System.out.println(pizza);
        pizza = PizzaFactory.createPineapplePizza(pizza);
        System.out.println(pizza);
    }
}
