package pl.sda.dp.dekorator.ex2;

public class PizzaFactory {

    public static IPizza createPineapplePizza(IPizza pizza) {
        return new CustomPizza(pizza, "ananas","cheese","ham","mushrooms");
    }

    public static IPizza createMargharitaPizza(IPizza pizza) {
        return new CustomPizza(pizza, "cheese");
    }

    public static IPizza createPepperoniPizza(IPizza pizza) {
        return new CustomPizza(pizza, "cheese", "pepperoni");
    }
}
