package pl.sda.dp.dekorator.ex2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CustomPizza implements IPizza {

    private IPizza pizza;
    private List<String> additionalIngredients = new ArrayList<>();

    public CustomPizza(IPizza pizza, String... additionalIngredients) {
        this.pizza = pizza;
        this.additionalIngredients.addAll(Arrays.asList(additionalIngredients));
    }

    @Override
    public double getPrice() {
        return pizza.getPrice() + additionalIngredients.size() * 3;
    }

    @Override
    public List<String> getIngredients() {
        List<String> newList = new ArrayList<>(additionalIngredients);
        newList.addAll(pizza.getIngredients());
        return newList;
    }

    @Override
    public String toString() {
        return "CustomPizza{" + "AdditionalIngredients=" + getIngredients() +
                ", new price= " + this.getPrice() + '}';
    }
}
