package pl.sda.dp.dekorator.ex2;

import java.util.List;

public interface IPizza {

    double getPrice();
    List<String> getIngredients();
}
