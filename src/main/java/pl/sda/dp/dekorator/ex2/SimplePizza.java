package pl.sda.dp.dekorator.ex2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SimplePizza implements IPizza{
    private double price = 10;
    private List<String> ingredients = new ArrayList<>(Arrays.asList("ciasto", "sos"));

    public SimplePizza(double price, String... ingredients) {
        this.price = price;
        this.ingredients.addAll(Arrays.asList(ingredients));
    }

    public SimplePizza(double price) {
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public List<String> getIngredients() {
        return ingredients;
    }

    @Override
    public String toString() {
        return "SimplePizza{" +
                "price=" + price +
                ", ingredients=" + ingredients +
                '}';
    }
}
