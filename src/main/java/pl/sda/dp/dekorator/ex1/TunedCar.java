package pl.sda.dp.dekorator.ex1;

public class TunedCar implements ICar{

    private ICar c;
    private boolean extraCharger;
    private boolean extraEngine;
    private boolean extraSeat;


    public TunedCar(ICar c, boolean extraCharger, boolean extraEngine, boolean extraSeat) {
        this.c = c;
        if (c.isHasCharger()) {
            this.extraCharger = false;
        } else {
            this.extraCharger = extraCharger;
        }
            this.extraEngine = extraEngine;
            this.extraSeat = extraSeat;

    }

    @Override
    public double getChargePressure() {
        return c.getChargePressure() * (extraCharger ? 1.1 : 1.0) * (extraEngine ? 1.2 : 1.0);
    }

    @Override
    public double getEngineCapacity() {
        return c.getEngineCapacity() * (extraEngine? 1.2 : 1.0);
    }

    @Override
    public int getSeatsNumber() {
        return c.getSeatsNumber() + (extraSeat? 1 : 0);
    }

    @Override
    public boolean isHasCharger() {
        return extraCharger || c.isHasCharger();
    }

    @Override
    public double getHorsePower() {
        return c.getHorsePower() * (extraCharger ? 1.1 : 1.0) * (extraEngine ? 1.2 : 1.0) * (extraSeat? 1:0.5);
    }

    @Override
    public String toString() {
        return "TunedCar{" +
                "chargePressure=" + this.getChargePressure() +
                ", engineCapacity=" + this.getEngineCapacity() +
                ", seatsNumber=" + this.getSeatsNumber() +
                ", hasCharger=" + isHasCharger() +
                ", horsePower=" + this.getHorsePower() +
                '}';
    }
}
