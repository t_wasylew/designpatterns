package pl.sda.dp.dekorator.ex1;

public class Car implements ICar{

    private double chargePressure;
    private double engineCapacity;
    private int seatsNumber;
    private boolean hasCharger;
    private double horsePower;

    public Car(double chargePressure, double engineCapacity, int seatsNumber, boolean hasCharger, double horsePower) {
        this.chargePressure = chargePressure;
        this.engineCapacity = engineCapacity;
        this.seatsNumber = seatsNumber;
        this.hasCharger = hasCharger;
        this.horsePower = horsePower;
    }

    @Override
    public String toString() {
        return "Car{" +
                "chargePressure=" + chargePressure +
                ", engineCapacity=" + engineCapacity +
                ", seatsNumber=" + seatsNumber +
                ", hasCharger=" + hasCharger +
                ", horsePower=" + horsePower +
                '}';
    }

    public double getChargePressure() {
        return chargePressure;
    }



    public double getEngineCapacity() {
        return engineCapacity;
    }



    public int getSeatsNumber() {
        return seatsNumber;
    }



    public boolean isHasCharger() {
        return hasCharger;
    }



    public double getHorsePower() {
        return horsePower;
    }


}
