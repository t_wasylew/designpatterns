package pl.sda.dp.dekorator.ex1;

public class CarShop {

    public ICar addExtraCharger(ICar c) {
        return new TunedCar(c, true, false,false);
    }

    public ICar addExtraEngine(ICar car) {
        return new TunedCar(car, false, true,false);
    }

    public ICar addExtraSeat(ICar car) {
        return new TunedCar(car, false, false,true);
    }

}
