package pl.sda.dp.dekorator.ex1;

public class Main {

    public static void main(String[] args) {
        Car car = new Car(1000d, 1000d, 2, false, 2000d);
        CarShop carShop = new CarShop();

        ICar car1 = carShop.addExtraEngine(car);
        car1 = carShop.addExtraEngine(car1);
        car1 = carShop.addExtraCharger(car1);
        car1 = carShop.addExtraSeat(car1);
        System.out.println(car1);
    }
}
