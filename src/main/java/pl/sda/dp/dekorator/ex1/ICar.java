package pl.sda.dp.dekorator.ex1;

public interface ICar {

    double getChargePressure();
    double getEngineCapacity();
    int getSeatsNumber();
    boolean isHasCharger();
    double getHorsePower();
}
