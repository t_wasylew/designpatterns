package pl.sda.dp.threads.synchronizacja;

public class BankAccount {

    private double money;

    public BankAccount(double money) {
        this.money = money;
    }

    public void addMoney(double ile) {
        money = money + ile;
    }

    public void subMoney(double ile) {
        money = money - ile;
    }

    public void balance() {
        System.out.println(money);
    }
}
