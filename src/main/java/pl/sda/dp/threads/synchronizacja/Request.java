package pl.sda.dp.threads.synchronizacja;

public class Request implements Runnable{
    private BankAccount bankAccount;
    private double iloscPieniedzy;
    private String kierunek;
    private BankAccount lockAccount;

    public Request(BankAccount bankAccount, double iloscPieniedzy, String kierunek) {
        this.bankAccount = bankAccount;
        this.iloscPieniedzy = iloscPieniedzy;
        this.kierunek = kierunek;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        synchronized (lockAccount) {
            if (kierunek.equalsIgnoreCase("add")) {
                bankAccount.addMoney(iloscPieniedzy);
            } else {
                bankAccount.subMoney(iloscPieniedzy);
            }
        }
    }
}
