package pl.sda.dp.threads.synchronizacja;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Bank {

    private BankAccount bankAccount;
    private ExecutorService service = Executors.newFixedThreadPool(5);

    public Bank(BankAccount bankAccount) {
        this.bankAccount = bankAccount;
    }

    public void sendAddToAccountRequest(double howMuch) {
        service.submit(new Request(bankAccount, howMuch, "add"));
    }

    public void sendSubstractRequest(double howMuch){
        service.submit(new Request(bankAccount, howMuch, "sub"));
    }

    public void balance() {
        bankAccount.balance();
    }

}
