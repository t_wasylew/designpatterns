package pl.sda.dp.threads.asynchronous_calculator;

public interface ICount {
    void calculateFactorial(int number);
}
