package pl.sda.dp.threads.asynchronous_calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class AsynchronousFactorialCalculator implements ICount {

    private List<String> valueString = new ArrayList<>();

    @Override

    public void calculateFactorial(int number) {
        ExecutorService service = Executors.newScheduledThreadPool(1);
        service.submit(new Request(number, this));
    }

    public void addToList(String value) {
        valueString.add(value);
    }

    public void printList() {
        for (String value: valueString) {
            System.out.println(value);
        }
    }
}
