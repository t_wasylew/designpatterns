package pl.sda.dp.threads.asynchronous_calculator;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        AsynchronousFactorialCalculator asynchronousFactorialCalculator = new AsynchronousFactorialCalculator();
        do {
            System.out.println("Write a number or type print (print all results) or quit to exit: ");
            String scanner1 = scanner.nextLine();
            if (scanner1.trim().startsWith("quit")) {
                System.out.println("Quitting");
                System.exit(0); // Pozwala na zakonczenie calej aplikacji z poziomu maina
            } else if (scanner1.trim().startsWith("print")) {
                asynchronousFactorialCalculator.printList();
                break;
            }else{
                int number = Integer.parseInt(scanner1.trim());
                asynchronousFactorialCalculator.calculateFactorial(number);
            }
        } while (true);

    }
}
