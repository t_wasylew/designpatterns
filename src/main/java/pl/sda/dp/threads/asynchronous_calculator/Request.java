package pl.sda.dp.threads.asynchronous_calculator;

public class Request implements Runnable {

    private int number;
    private AsynchronousFactorialCalculator asynchronousFactorialCalculator;

    public Request(int number, AsynchronousFactorialCalculator asynchronousFactorialCalculator) {
        this.number = number;
        this.asynchronousFactorialCalculator = asynchronousFactorialCalculator;
    }

    @Override
    public void run() {
        int value = 1;
        for (int i = 1; i <= number; i++) {
            value *= i;
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Result: "+ value);
        asynchronousFactorialCalculator.addToList(number +"! = " + value);
    }

}
