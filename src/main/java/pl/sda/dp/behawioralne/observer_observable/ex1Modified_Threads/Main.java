package pl.sda.dp.behawioralne.observer_observable.ex1Modified_Threads;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        SmsStation smsStation = new SmsStation();
        smsStation.addPhone("123456789");
        smsStation.addPhone("987654321");
        smsStation.addPhone("123789456");
        smsStation.addPhone("123");
        smsStation.addPhone("321");

        while (true) {
            Scanner scanner = new Scanner(System.in);
            System.out.print("Podaj numer telefonu a nastepnie wpisz tresc wiadomosci: ");
            String massage = scanner.nextLine();
            String[] spliMassage = massage.trim().split(" ");
            String numer = spliMassage[0];
            String tresc = spliMassage[1];
            smsStation.sendSms(numer, tresc);
        }
    }
}
