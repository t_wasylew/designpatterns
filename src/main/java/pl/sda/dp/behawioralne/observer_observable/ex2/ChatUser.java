package pl.sda.dp.behawioralne.observer_observable.ex2;


import java.util.LinkedList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class ChatUser implements Observer {
    private int id;
    private String nick;
    private List<String> massages = new LinkedList<>();
    private boolean isAdmin;

    public ChatUser(int id, String nick) {
        this.id = id;
        this.nick = nick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }

    public List<String> getMassages() {
        return massages;
    }

    public void setMassages(List<String> massages) {
        this.massages = massages;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Massage) {
            Massage massage = (Massage) arg;
            if (massage.getUserId() == this.getId()) {
                massages.add(massage.getMassage());
                System.out.println(massage.getMassage());
            }
        }
    }
}
