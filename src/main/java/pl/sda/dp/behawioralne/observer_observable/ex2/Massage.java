package pl.sda.dp.behawioralne.observer_observable.ex2;

public class Massage {

    private int userId;
    private String massage;

    public Massage(int userId, String massage) {
        this.userId = userId;
        this.massage = massage;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}
