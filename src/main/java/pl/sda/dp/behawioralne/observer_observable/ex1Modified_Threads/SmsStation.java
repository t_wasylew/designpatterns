package pl.sda.dp.behawioralne.observer_observable.ex1Modified_Threads;

import java.util.Observable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SmsStation extends Observable implements ISender {


    public void addPhone(String nuber) {
        addObserver(new Phone(nuber));
    }

    public void sendSms(String number, String tresc) {
        ExecutorService service = Executors.newFixedThreadPool(5);
        Request request = new Request(this, new Massage(number, tresc));
        service.submit(request);
    }

    public void sendSms(Massage massage) {
        setChanged();
        notifyObservers(massage);
    }
}
