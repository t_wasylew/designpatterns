package pl.sda.dp.behawioralne.observer_observable.ex1Modified_Threads;

public interface ISender {
    void sendSms(Massage massage);
}
