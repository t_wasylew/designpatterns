package pl.sda.dp.behawioralne.observer_observable.ex2;

import java.util.*;

public class ChatRoom extends Observable {

    private static int id = 0;
    private String roomName;
    private static final List<String> admini = new ArrayList<>(Arrays.asList("Admin", "Administrator", "Admin1", "Admin2"));
    private Map<Integer, ChatUser> userMap = new HashMap<>();

    public ChatRoom(String roomName) {
        this.roomName = roomName;
    }

    public void userLogin(String nick) {
        for (ChatUser user : userMap.values()) {
            if (user.getNick().equals(nick)) {
                System.out.println("Taki nick juz istnieje");
                return;
            }
        }
        ChatUser user = new ChatUser(++id, nick);
        if(admini.contains(nick)) {
            user.setAdmin(true);
            System.out.println("Ustawiam admina");
        }
        userMap.put(id, user);
        addObserver(user);
    }

    public void sendMassage(int userId, String massage) {
        Massage massage1 = new Massage(userId, massage);
            setChanged();
            notifyObservers(massage1);
    }

    public void kickUser(int id_Kick, int id_Admin) {

        if (!userMap.containsKey(id_Kick) || !userMap.containsKey(id_Admin)) {
            System.out.println("Nie ma jednego z tych Id");
            return;
        }
        if(userMap.get(id_Admin).isAdmin()){
            System.out.println("Usunieto usera: " + userMap.get(id_Kick).getNick());
            deleteObserver(userMap.get(id_Kick));
            userMap.remove(id_Kick);
            id--;
        }else {
            System.out.println("Nie masz uprawnien do usuwania uzytkownikow");
        }
    }
}
