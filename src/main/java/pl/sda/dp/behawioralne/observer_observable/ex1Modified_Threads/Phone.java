package pl.sda.dp.behawioralne.observer_observable.ex1Modified_Threads;

import java.util.Observable;
import java.util.Observer;

public class Phone implements Observer {

    private String nrTelefonu;

    public Phone(String nrTelefonu) {
        this.nrTelefonu = nrTelefonu;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Massage) {
            Massage massage = (Massage) arg;
            if (massage.getNumer().equals(nrTelefonu)) {
                System.out.println("Otrzymales wiadomosc: " + massage.getTresc());
            }
        }
    }
}
