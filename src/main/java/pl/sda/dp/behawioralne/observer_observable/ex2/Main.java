package pl.sda.dp.behawioralne.observer_observable.ex2;

import java.util.Scanner;
import java.util.WeakHashMap;

public class Main {

    public static void main(String[] args) {
        ChatRoom chatRoom = new ChatRoom("SDA");
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Wpisz komende: wyslij: idOdbiorcy - tresc wiadomosci/ zaloguj: nick usera/ usun: idUsuwanego idAdmina");
            String komenda = scanner.nextLine();
            String[] splitKomenda = komenda.split(":");
            switch (splitKomenda[0].trim()) {
                case "zaloguj":
                    chatRoom.userLogin(splitKomenda[1].trim());
                    break;
                case "wyslij":
                    String[] podzielonySplit = splitKomenda[1].trim().split("-");
                    chatRoom.sendMassage(Integer.parseInt(podzielonySplit[0].trim()), podzielonySplit[1].trim());
                    break;
                case "usun":
                    String[] podzieloneId = splitKomenda[1].trim().split(" ");
                    chatRoom.kickUser(Integer.parseInt(podzieloneId[0]), Integer.parseInt(podzieloneId[1]));
                    break;
            }
        }

    }
}
