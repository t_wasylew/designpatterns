package pl.sda.dp.behawioralne.observer_observable.ex1Modified_Threads;

public class Request implements Runnable{

    private ISender station;
    private Massage massage;

    public Request(ISender station, Massage massage) {
        this.station = station;
        this.massage = massage;
    }

    public ISender getStation() {
        return station;
    }

    public void setStation(ISender station) {
        this.station = station;
    }

    public Massage getMassage() {
        return massage;
    }

    public void setMassage(Massage massage) {
        this.massage = massage;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(massage.getTresc().length() * 100);
            System.out.println("Text coded");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        station.sendSms(massage);

    }
}
