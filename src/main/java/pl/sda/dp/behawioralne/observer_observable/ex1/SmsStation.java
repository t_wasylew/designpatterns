package pl.sda.dp.behawioralne.observer_observable.ex1;

import java.util.Observable;

public class SmsStation extends Observable {


    public void addPhone(String nuber) {
        addObserver(new Phone(nuber));
    }

    public void sendSms(String number, String tresc) {
        Massage massage = new Massage(number, tresc);
        setChanged();
        notifyObservers(massage);
    }
}
