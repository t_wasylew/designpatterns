package pl.sda.dp.behawioralne.observer_observable.ex1;

public class Massage {

    private String numer;
    private String tresc;

    public Massage(String numer, String tresc) {
        this.numer = numer;
        this.tresc = tresc;
    }

    public String getNumer() {
        return numer;
    }

    public void setNumer(String numer) {
        this.numer = numer;
    }

    public String getTresc() {
        return tresc;
    }

    public void setTresc(String tresc) {
        this.tresc = tresc;
    }
}
